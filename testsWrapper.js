import supertest from "supertest";

import operators from "./fixtures/operators.js";

export const request = supertest("http://wis14.np.ua/");
export const token = await getToken();

function login() {
	const operatorLogin = operators.regularBranchOperator.login;
	const operatorPassword = operators.regularBranchOperator.password;

	const body = {
		method: "login",
		params: {
			username: operatorLogin,
			password: operatorPassword,
		},
		id: 123,
	};
	const headers = {
		Connection: "keep-alive",
		"Content-Type": "application/json",
	};

	return request.post("rpc-api-develop/v2.0/User").send(body).set(headers);
}

async function getToken() {
	let token;
	await login()
		.then((response) => {
			const responseData = JSON.parse(response.text);
			token = responseData.result.SessionToken;
		})
		.catch((error) => {
			console.log(error);
		});
	return token;
}
