import { expect } from "chai";

import operators from "../fixtures/operators.js";
import { request, token } from "../testsWrapper.js";

xdescribe("Проверка списка доступных отделений для входа/выхода", () => {
	const operatorID = operators.regularBranchOperator.id;
	const operatorName = operators.regularBranchOperator.fullName;

	it("getAvailableWarehousesForInOutFixation", (done) => {
		const requestHeaders = {
			Connection: "keep-alive",
			"Content-Type": "application/json",
			"x-awis-session-token": token,
		};
		const requestBody = {
			method: "getAvailableWarehousesForInOutFixation",
			params: {
				user: {
					_type: "Ref",
					object: "Catalog.Users",
					id: operatorID,
					name: operatorName,
				},
			},
			id: "555",
		};
		request
			.post("rpc-api-develop/v2.0/User")
			.send(requestBody)
			.set(requestHeaders)
			.expect(200)
			.then((response) => {
				const responseText = JSON.parse(response.text);
				const availableWarehousesArr = responseText.result.AvailableWarehouses;
				expect(availableWarehousesArr).to.have.lengthOf.above(0);
				done();
			})
			.catch((error) => {
				console.log(error);
				done(error);
			});
	});
});
