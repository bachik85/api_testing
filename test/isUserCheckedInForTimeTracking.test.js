import { expect } from "chai";

import operators from "../fixtures/operators.js";
import { request, token } from "../testsWrapper.js";

xdescribe("Проверка фиксация рабочего времени", () => {
	const operatorID = operators.regularBranchOperator.id;
	const operatorName = operators.regularBranchOperator.fullName;
	const warehouseID = operators.regularBranchOperator.warehouse.id;
	const warehouseName = operators.regularBranchOperator.warehouse.name;
	const eventType = {
		login: "Entry",
		logOut: "Exit",
	};

	const requestHeaders = {
		Connection: "keep-alive",
		"Content-Type": "application/json",
		"x-awis-session-token": token,
	};

	const requestBody = (event) => {
		return {
			method: "writeTimeTrackingEvent",
			params: {
				event: event,
				user: {
					_type: "Ref",
					object: "Catalog.Users",
					id: operatorID,
					name: operatorName,
				},
				warehouse: {
					_type: "Ref",
					object: "Catalog.Warehouses",
					id: warehouseID,
					name: warehouseName,
				},
			},
			id: 123,
		};
	};

	it("writeTimeTrackingEvent (Entry)", (done) => {
		setTimeout(() => {
			request
				.post("rpc-api-develop/v2.0/User")
				.send(requestBody(eventType.login))
				.set(requestHeaders)
				.expect(200)
				.then((response) => {
					const successMessage = "Працівник вже зафіксований на вхід. В повторному вході немає сенсу.";
					const responseText = JSON.parse(response.text);
					const responseMessage = responseText.result.AdditionalInfo;
					expect(responseMessage).to.not.be.equal(successMessage);
					done();
				})
				.catch((error) => {
					console.log(error);
					done(error);
				});
		}, 60000);
	});

	it("isUserCheckedInForTimeTracking", (done) => {
		const body = {
			method: "isUserCheckedInForTimeTracking",
			params: {
				user: {
					_type: "Ref",
					object: "Catalog.Users",
					id: operatorID,
					name: operatorName,
				},
			},
			id: "123",
		};

		request
			.post("rpc-api-develop/v2.0/User")
			.send(body)
			.set(requestHeaders)
			.expect(200)
			.then((response) => {
				const responseResult = JSON.parse(response.text);
				const result = responseResult.result;
				expect(result).to.be.true;
				done();
			})
			.catch((error) => {
				console.log(error);
				done(error);
			});
	});

	it("writeTimeTrackingEvent (Exit)", (done) => {
		setTimeout(() => {
			request
				.post("rpc-api-develop/v2.0/User")
				.send(requestBody(eventType.logOut))
				.set(requestHeaders)
				.expect(200)
				.then((response) => {
					const successMessage = `Увага! Зафіксовано Вихід з роботи користувача ${operatorName} успішно `;
					// const successMessage1 = "Працівник вже зафіксований на вхід. В повторному вході немає сенсу.";
					// const successMessage2 = "Вихід працівника вже було зафіксовано. В повторному виході немає сенсу.";
					// const successMessage2 = "Вихід з роботи не зафіксовано. Спробуйте через 1хв.";
					const responseText = JSON.parse(response.text);
					const responseMessage = responseText.result.AdditionalInfo;
					expect(responseMessage).to.be.equal(successMessage);
					// expect(responseMessage).to.be.equal(successMessage1);
					done();
				})
				.catch((error) => {
					console.log(error);
					done(error);
				});
		}, 60000);
	});
});
