import { expect } from "chai";
import supertest from "supertest";

const request = supertest("https://gorest.co.in/public/v1/");
const TOKEN = "211a8f98007a96ec7ec0dfaf16845a55722591b2edc38267627f0b7c128d2742";

describe("Users", (done) => {
	it("GET/users", () => {
		request.get(`users?access-token=${TOKEN}`).then((res) => {
			expect(res.body.data).to.not.be.empty;
			expect(200);
			done();
		});
	});
});
