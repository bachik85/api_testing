export default {
	regularBranchOperator: {
		id: "29d28aea-5d80-11e8-9d95-005056b2fc3d",
		login: "plaksyvyi.a",
		password: "123",
		fullName: "Плаксивий Антон Павлович",
		warehouse: {
			id: "83751fbc-60f0-11e0-82bc-0026b97ed48a",
			name: "Відділення №2 (до 30 кг): вул. Європейська, 31",
		},
	},
	regularBranchHead: {
		id: "ae7110f0-4e69-11ea-86dd-005056b24375",
		login: "sushko.va",
		password: "123",
		fullName: "Сушко Вікторія Аркадіївна",
		warehouse: {
			id: "7ddcc503-c432-11e1-86b4-0026b97ed48a",
			name: "Відділення №58 (до 30 кг): вул. Виборзька, 49",
		},
	},
};
